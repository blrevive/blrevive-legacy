using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Utils;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.Linq;
using System.ComponentModel;
using Serilog;

namespace Launcher.UI.Tabs
{
    public class SettingsTab : UserControl
    {
        public SettingsTab()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnRegisterClientClick(object sender, RoutedEventArgs e)
        {
            RegisterClientWindow w = new RegisterClientWindow()
            {
                DataContext = this.DataContext
            };
            w.Closing += (object sender,  CancelEventArgs e) => {
                //LauncherViewDataProvider.UpdateWindow(this);
                MainWindow.UpdateClientSelects();
            };
            w.Show();
            
        }

        public void OnRemoveClientClick(object sender, RoutedEventArgs e)
        {
            var ClientListBox = this.Find<ListBox>("SettingsTabClientList");

            if(ClientListBox.ItemCount == 0)
                return;
            
            GameRegistry.ClientInfo client = ((GameRegistry.ClientInfo)ClientListBox.SelectedItem);
            GameRegistry.RemoveClient(c => c.Alias == client.Alias);
            //LauncherViewDataProvider.UpdateWindow(this);
            MainWindow.UpdateClientSelects();
        }
    }
}