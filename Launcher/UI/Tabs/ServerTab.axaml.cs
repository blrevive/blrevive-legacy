using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.IO;

namespace Launcher.UI.Tabs
{
    public class ServerTab : UserControl
    {
        protected TextBox ServerNameInput;
        protected TextBox ServerAddressInput;
        protected NumericUpDown ServerPortInput;
        protected ComboBox ClientInput;
        protected ComboBox PlaylistInput;
        protected ComboBox GamemodeInput;
        protected ComboBox MapInput;
        protected NumericUpDown MaxPlayersInput;
        protected NumericUpDown BotCountInput;
        protected TextBox CustomParamsInput;

        public ServerTab()
        {
            InitializeComponent();
            
            ServerNameInput = this.Find<TextBox>("ServerName");
            ServerAddressInput = this.Find<TextBox>("ServerAddress");
            ServerPortInput = this.Find<NumericUpDown>("ServerPort");
            ClientInput = this.Find<ComboBox>("ClientSelect");
            PlaylistInput = this.Find<ComboBox>("PlaylistSelect");
            GamemodeInput = this.Find<ComboBox>("GamemodeSelect");
            MapInput = this.Find<ComboBox>("MapSelect");
            MaxPlayersInput = this.Find<NumericUpDown>("MaxPlayers");
            BotCountInput = this.Find<NumericUpDown>("BotCount");
            CustomParamsInput = this.Find<TextBox>("CustomParams");
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnLaunchClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = (GameRegistry.ClientInfo)ClientInput.SelectedItem;

                GameInstanceManager.StartServer(new GameInstanceManager.ServerStartOptions(){
                    Filename = client.PatchedGameFile,
                    BinaryPath = Path.Join(client.InstallPath, client.BinaryDir),
                    Address = ServerAddressInput.Text,
                    Port = (int)ServerPortInput.Value,
                    Servername = ServerNameInput.Text,
                    Map = (string)MapInput.SelectedItem,
                    Playlist = (string)PlaylistInput.SelectedItem,
                    Gamemode = (string)GamemodeInput.SelectedItem,
                    BotCount = (int)BotCountInput.Value,
                    MaxPlayers = (int)MaxPlayersInput.Value,
                    CustomParams = CustomParamsInput.Text
                });
            } catch(UserInputException ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            }
        }
    }
}