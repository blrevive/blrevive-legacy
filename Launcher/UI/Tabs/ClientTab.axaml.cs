using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Collections.Generic;
using Launcher.Configuration;
using Serilog;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.Linq;
using System.IO;

namespace Launcher.UI.Tabs
{ 
    public class ClientTab : UserControl
    {
        private ListBox LobbySelect;

        public ClientTab()
        {
            InitializeComponent();
            LobbySelect = this.Find<ListBox>("LobbyList");
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnServerAddressSaveClick(object sender, RoutedEventArgs e)
        {
            var AddressTextBox = this.Find<TextBox>("ServerAddress");
            var PortNum = this.Find<NumericUpDown>("ServerPort");
            string Address = AddressTextBox.Text;
            try 
            {        
                int Port = (int)PortNum.Value;
                if(Address == null || Port == 0)
                    throw new UserInputException("Address and port must be specified!");

                var lobbies = Config.ServerList.Hosts;
                foreach(var lobby in lobbies)
                    if(lobby.Address == AddressTextBox.Text && lobby.Port == (int)PortNum.Value)
                        throw new UserInputException("The server is already added to your list!");
                Config.ServerList.Hosts.Add(new Server(Address, Port, true));
                Config.Save();
                LauncherViewDataProvider.UpdateWindow(this);
            } catch(UserInputException ex) {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            }
        }

        public void OnLobbySelect(object sender, SelectionChangedEventArgs e)
        {
            Config.ServerList.PreviousHost = (Server)LobbySelect.SelectedItem;
            Config.Save();
            LauncherViewDataProvider.UpdateWindow(this);
        }

        public void OnLobbyRemove(object sender, RoutedEventArgs e)
        {
            Server selected = (Server)LobbySelect.SelectedItem;
            Config.ServerList.Hosts = ((Server[])LobbySelect.Items).Where(l => !l.Equals(selected)).ToList();
            if(Config.ServerList.Hosts.Count != 0)
            {
                Config.ServerList.PreviousHost = Config.ServerList.Hosts[0];
                LobbySelect.SelectedItem = Config.ServerList.PreviousHost;
            }
            Config.Save();
            LauncherViewDataProvider.UpdateWindow(this);
        }

        public void OnLaunchClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var ClientComboBox = this.Find<ComboBox>("ClientSelect");
                var AddressTextBox = this.Find<TextBox>("ServerAddress");
                var PortNum = this.Find<NumericUpDown>("ServerPort");
                var UserNameTextBox = this.Find<TextBox>("UserName");
                var CustomParamsTextBox = this.Find<TextBox>("CustomParams");

                GameRegistry.ClientInfo client = (GameRegistry.ClientInfo)ClientComboBox.SelectedItem;
                if(client == null)
                    throw new UserInputException("A Client must be selected!");

                string Address = AddressTextBox.Text;
                int Port = (int)PortNum.Value;
                string Username = UserNameTextBox.Text;
                string CustomParams = CustomParamsTextBox.Text;

                GameInstanceManager.StartClient(new GameInstanceManager.ClientStartOptions(){
                    Filename = client.PatchedGameFile,
                    BinaryPath = Path.Join(client.InstallPath, client.BinaryDir),
                    IP = Address,
                    Port = Port,
                    Playername = Username,
                    CustomParams = CustomParams
                });
            } catch(UserInputException ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            }
        }
        
        public void OnLobbyListBackup(object sender, RoutedEventArgs e)
        {
            List<Server> servers = Config.ServerList.Hosts;
            List<Server> customServers = servers.Where(s => s.CustomServer).ToList();
            Config.Hosts.Hosts = customServers;
            Config.Save(Config.Hosts);
            LauncherViewDataProvider.UpdateWindow(this);
        }

        public void OnLobbyListRestore(object sender, RoutedEventArgs e)
        {
            List<Server> servers = Config.ServerList.Hosts;
            List<Server> foreignServers = servers.Where(s => !s.CustomServer).ToList();
            Config.ServerList.Hosts = Config.Hosts.Hosts;
            Config.Save();
            LauncherViewDataProvider.UpdateWindow(this);
        }
    }
}