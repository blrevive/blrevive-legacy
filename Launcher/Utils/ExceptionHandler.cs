using System;
using Serilog;
using Avalonia;

namespace Launcher.Utils
{
    /// <summary>
    /// A wrapper for exceptions that require user attention but doesn't crash or invalidate the application.
    /// </summary>
    public class UserInputException : Exception
    {
        public UserInputException() : base() {}
        public UserInputException(string message) : base (message) {}
        public UserInputException(string message, Exception inner) : base(message, inner) {}
    }
}